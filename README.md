Integration testing
===================

This project is about converting color values. It currently supports:

- rgb-to-hex

- hex-to-rgb
