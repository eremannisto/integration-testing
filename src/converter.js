/**
 * Ternary operator to pad single digit HEX values
 * @param {*} hex 
 * @returns 
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

// Module exports:
module.exports = {

    /**
     *  Converts RGB values to HEX string 
     *  @param {number} red - 0-255
     *  @param {number} green - 0-255
     *  @param {number} blue - 0-255
     *  @returns {string} - HEX string
    */
    rgbToHex: (red, green, blue) => {
        const redHex    = red.toString(16);
        const greenHex  = green.toString(16);
        const blueHex   = blue.toString(16);
        return "#" + pad(redHex) + pad(greenHex) + pad(blueHex);
    },

    /** 
     *  Converts HEX string to RGB values
     *  @param {string} hex - HEX string
     *  @returns {object} - RGB values
     */

    hexToRgb: (hex) => {

        // If there is a # in the string, remove it
        if (hex.charAt(0) === "#") {
            hex = hex.substring(1);
        }
    
        const redHex    = hex.substring(0, 2);
        const greenHex  = hex.substring(2, 4);
        const blueHex   = hex.substring(4, 6);
        return {
            red:    parseInt(redHex, 16),
            green:  parseInt(greenHex, 16),
            blue:   parseInt(blueHex, 16)
        }
    }
};