const express = require('express');
const converter = require('./converter');
const app = express();
const PORT = 3000;

app.get('/', (req, res) => res.send("Welcome!"));

// RGB to HEX endpoint
app.get('/rgb-to-hex', (req, res) => {
    const red   = parseInt(req.query.r);
    const green = parseInt(req.query.g);
    const blue  = parseInt(req.query.b);
    const hex   = converter.rgbToHex(red, green, blue);
    res.send(hex);
});

// HEX to RGB endpoint
app.get('/hex-to-rgb', (req, res) => {
    const hex   = req.query.hex;
    const rgb   = converter.hexToRgb(hex);
    res.send(JSON.stringify(rgb));
});


process.env.NODE_ENV === "test" 
    ? module.exports = app 
    : app.listen(PORT, () => console.log(`Listening on port ${PORT}...`));

console.log("NODE_ENV: " + process.env.NODE_ENV);

