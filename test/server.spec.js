const expect    = require('chai').expect;
const request   = require('request');
const app       = require('../src/server');
const PORT      = 3000;

describe('Color Code Converter API', () => {
    let server;

    before("Start server", (done) => {
        server = app.listen(PORT, () => {
            done();
        });
    });

    describe('RGB to Hex conversion', () => {
        const baseurl = `http://localhost:${PORT}/rgb-to-hex?`;
        it('returns status 200', (done) => {
            request(baseurl + 'r=255&g=255&b=255', (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
    });

    describe('Hex to RGB conversion', () => {
        const baseurl = `http://localhost:${PORT}/hex-to-rgb?`;
        it('returns status 200', (done) => {
            request(baseurl + 'hex=#ffffff', (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
    });

    after("Stop server", (done) => {
        server.close();
        done();
    });
    
});
