// TDD - unit testing

const expect = require('chai').expect;
const converter = require('../src/converter');

describe('Converter', () => {

    describe('RGB to Hex conversion', () => {

        it('converts the basic colors', () => {
            const redHex    = converter.rgbToHex(255, 0, 0);
            const greenHex  = converter.rgbToHex(0, 255, 0);
            const blueHex   = converter.rgbToHex(0, 0, 255);

            expect(redHex).to.equal('#ff0000');
            expect(greenHex).to.equal('#00ff00');
            expect(blueHex).to.equal('#0000ff');
        });

        it('converts black', () => {
            const blackHex = converter.rgbToHex(0, 0, 0);
            expect(blackHex).to.equal('#000000');
        });

        it('converts white', () => {
            const whiteHex = converter.rgbToHex(255, 255, 255);
            expect(whiteHex).to.equal('#ffffff');
        });

    });

    describe('Hex to RGB conversion', () => {

        it('converts the basic colors', () => {
            const redRGB    = converter.hexToRgb('#ff0000');
            const greenRGB  = converter.hexToRgb('#00ff00');
            const blueRGB   = converter.hexToRgb('#0000ff');

            expect(redRGB).to.deep.equal({ red: 255, green: 0, blue: 0 });
            expect(greenRGB).to.deep.equal({ red: 0, green: 255, blue: 0 });
            expect(blueRGB).to.deep.equal({ red: 0, green: 0, blue: 255 });
        });

        it('converts black', () => {
            const blackRGB = converter.hexToRgb('#000000');
            expect(blackRGB).to.deep.equal({ red: 0, green: 0, blue: 0 });
        });

        it('converts white', () => {
            const whiteRGB = converter.hexToRgb('#ffffff');
            expect(whiteRGB).to.deep.equal({ red: 255, green: 255, blue: 255 });
        });

    });

});